package util;

public class Start {

	public static void main(String[] args) {
		GenerateSimplePdfReportWithJasperReports relatorio = new GenerateSimplePdfReportWithJasperReports();
		try {
			relatorio.imprimir(Integer.valueOf(args[0]));
		} catch (Exception e) {
			System.out.println("Erro ao imprimir relatorio: " + e.getMessage());
		}

	}

}
