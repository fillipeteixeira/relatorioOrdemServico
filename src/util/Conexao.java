package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

	private static Connection connection;
	
	public static Connection getConexao() {
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Erro ao assinar driver de conex�o com o banco: "+e.getMessage());
		}
		
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://ec2-23-21-162-90.compute-1.amazonaws.com:5432/d2gc9oocb8ptot", "izawnnstjedcmu",
					"ac4c44d625f52a210744ae4defa486ec920f6478032117d2c2cb24ae075f185d");
		
		} catch (SQLException e) {
			System.out.println("Erro ao conectar no banco: "+e.getMessage());
		}
		
		
		if (connection != null) {
			return connection;
		} else {
			System.out.println("N�o foi possivel obter conex�o com o banco de dados!");
		}
		
		return null;
	}
}
