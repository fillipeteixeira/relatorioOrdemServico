package util;

import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class GenerateSimplePdfReportWithJasperReports {
	Connection connection = null;
	InputStream input;

	// Recupera os caminhos para que a classe possa encontrar os relatórios
	public GenerateSimplePdfReportWithJasperReports() {
		String path = "RelatorioOrdemServico.jrxml";
		input = this.getClass().getResourceAsStream(path);

	}

	// Imprime/gera uma lista de Clientes
	public void imprimir(Integer numeroOs) throws Exception {

		JasperReport report = JasperCompileManager.compileReport(input);
		connection = Conexao.getConexao();
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("pcodigo_os", Long.parseLong(String.valueOf(numeroOs)));
		JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);

		JasperExportManager.exportReportToPdfFile(print, "c:/temp/Relatorio_Ordem_de_Servico.pdf");
	}

}